import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class YellingTest {
	Yelling yell;
	String result;
	@Before
	public void setUp() throws Exception {
		yell = new Yelling();
	}

	@After
	public void tearDown() throws Exception {
	}
//R1: One person is yelling
	@Test
	public void testOnePersonYelling() {
		
		 result = yell.scream("Peter");
		assertEquals("Peter is yelling",result);
	}
	//R2:  person is yelling
		@Test
		public void nobodyIsYelling() {
			
			 result = yell.scream("");
			assertEquals("Nobody is yelling",result);
		}
		
		//R3: Uppercase PETER is yelling
			@Test
				public void testUppercaseYelling() {
				
				 result = yell.scream("PETER");
				assertEquals("PETER IS YELLING",result);
				}
				
		//R4: Two people are yelling
			@Test
				public void testTwoPeopleYelling() {
				
				 result = yell.scream("Peter,Kadeem");
				System.out.println(result);
				assertEquals("Peter and Kadeem are yelling",result);
				}
				
		//R5: More Than two people are yelling
			@Test
			    public void testMorePeopleYelling() {
				
				 result = yell.scream("Peter, Kadeem, Albert");
				System.out.println(result);
				assertEquals("Peter, Kadeem, and Albert are yelling",result);
				}
		
			//R6: Shouting at a lot of people
			@Test
			    public void testShoutingAtAlotOfPeople() {
				
				 result = yell.scream("Peter, Kadeem, Albert, EMAD");
				System.out.println(result);
				assertEquals("Peter, Kadeem, and Albert are yelling. SO IS EMAD!",result);
				}
				}
